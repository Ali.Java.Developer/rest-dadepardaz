package com.dade.restdadepardaz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestDadepardazApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestDadepardazApplication.class, args);
    }

}
