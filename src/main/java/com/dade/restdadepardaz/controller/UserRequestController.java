package com.dade.restdadepardaz.controller;

import com.dade.restdadepardaz.entity.User;
import com.dade.restdadepardaz.entity.UserRequest;
import com.dade.restdadepardaz.response.Response;
import com.dade.restdadepardaz.response.StatusResponse;
import com.dade.restdadepardaz.service.UserRequestService;
import com.dade.restdadepardaz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/request")
public class UserRequestController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRequestService userRequestService;

    @PostMapping("")
    public Response createRequest(@Valid @ModelAttribute UserRequest userRequest ,@Valid @RequestParam("nationalId") String nationalId) {
        Response response = null;
        UserRequest savedRequest = null;
        if (userRequestService.isExist(userRequest)){
            User user = userRequest.getUser();
            if(userService.isPeresent(nationalId)){
                User foundUser = userService.findUserByNationalId(nationalId);
                userRequest.setUser(foundUser);
                savedRequest = userRequestService.saveNewRequest(userRequest);
                response = new StatusResponse(200,"ok",savedRequest);
            }else {
                response = new StatusResponse(400,"Bad Request",savedRequest);
            }
        }else {
            response = new StatusResponse(400,"Bad Request",savedRequest);
        }
        return response;

    }
}
