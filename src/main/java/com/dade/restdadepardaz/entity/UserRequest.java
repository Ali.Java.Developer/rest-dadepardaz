package com.dade.restdadepardaz.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "request")
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class , property = "id")
public class UserRequest implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "type_cost" , nullable = false)
    @NotBlank(message = "type of cost must not be blank!")
    private String typeCost;
    private String describes;
    @Column(nullable = false)
    @NotNull(message = "cost must not be blank!")
    private int cost;

    private String cover;

    @Transient
    @JsonIgnore
    private MultipartFile file;
    @Column(nullable = false)
    @NotBlank(message = "shaba must not be blank!")
    private String shaba;

    @ManyToOne
    private User user;


}
