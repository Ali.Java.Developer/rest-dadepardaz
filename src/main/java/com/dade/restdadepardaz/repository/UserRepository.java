package com.dade.restdadepardaz.repository;

import com.dade.restdadepardaz.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    public Optional<User> findByNationalId(String nationalId);
}
