package com.dade.restdadepardaz.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class StatusResponse implements Response{
    @JsonProperty("http-code")
    private int httpCode;
    private String message;
    private Object object;

    public StatusResponse(int httpCode, String message) {
        this.httpCode = httpCode;
        this.message = message;
    }
}
