package com.dade.restdadepardaz.service;

import com.dade.restdadepardaz.entity.UserRequest;
import com.dade.restdadepardaz.repository.UserRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

@Service
public class UserRequestService {

    @Autowired
    private UserRequestRepository userRequestRepository;

    public boolean isExist(UserRequest userRequest) throws NullPointerException {
        boolean isExist = false;
        if (userRequest != null) {
            isExist = true;
        } else {
            isExist = false;
            throw new NullPointerException("User request is null!");
        }
        return isExist;
    }

    public UserRequest saveNewRequest(UserRequest userRequest) {
        if (userRequest.getFile() != null) {
            try {
                String path = ResourceUtils.getFile("classpath:static/files").getAbsolutePath();
                byte[] bytes = userRequest.getFile().getBytes();
                String name = UUID.randomUUID() + "." + userRequest.getFile().getContentType().split("/")[1];
                userRequest.setCover(name);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return userRequestRepository.save(userRequest);
    }
}
