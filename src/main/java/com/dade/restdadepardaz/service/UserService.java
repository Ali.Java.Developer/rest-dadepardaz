package com.dade.restdadepardaz.service;

import com.dade.restdadepardaz.entity.User;
import com.dade.restdadepardaz.entity.UserRequest;
import com.dade.restdadepardaz.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User findUserByNationalId(String nationalId){
        User foundUser = userRepository.findByNationalId(nationalId).get();
        return foundUser;
    }

    public boolean isPeresent(String nationalId) throws NoSuchElementException {
        Optional<User> userOptional = userRepository.findByNationalId(nationalId);
        boolean isPeresent = false;
        if(userOptional.isPresent()){
            isPeresent = true;
        }else {
            isPeresent = false;
            throw new NoSuchElementException("User is not find on database!");
        }
        return isPeresent;
    }

}
